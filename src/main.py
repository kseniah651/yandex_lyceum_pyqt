import sys
import random
import sqlite3
import os

from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtGui import QPixmap
from PyQt5 import QtCore, QtGui, QtWidgets


class UiWindow2:  # дизайн для окна со справкой об элементе
    def __init__(self):
        self.centralWidget = None
        self.gridLayout = None
        self.statusBar = None
        self.menuBar = None
        self.horizontalLayout = None
        self.label_img_info = None
        self.label_text_info = None
        self.label_name_info = None
        self.verticalLayout = None

    def setup_ui(self, main_window):
        main_window.setObjectName("MainWindow")
        main_window.setEnabled(True)
        main_window.resize(735, 549)
        size_policy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(main_window.sizePolicy().hasHeightForWidth())
        main_window.setSizePolicy(size_policy)
        main_window.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.centralWidget = QtWidgets.QWidget(main_window)
        self.centralWidget.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.centralWidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setContentsMargins(10, 10, 10, 10)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setContentsMargins(10, 10, 10, 10)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_name_info = QtWidgets.QLabel(self.centralWidget)
        font = QtGui.QFont()
        font.setPointSize(20)
        self.label_name_info.setFont(font)
        self.label_name_info.setAlignment(QtCore.Qt.AlignLeading
                                          | QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.label_name_info.setObjectName("label_name_info")
        self.verticalLayout.addWidget(self.label_name_info)
        self.label_text_info = QtWidgets.QLabel(self.centralWidget)
        self.label_text_info.setAlignment(QtCore.Qt.AlignLeading
                                          | QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.label_text_info.setObjectName("label_text_info")
        self.label_text_info.setWordWrap(True)
        self.verticalLayout.addWidget(self.label_text_info)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.label_img_info = QtWidgets.QLabel(self.centralWidget)
        self.label_img_info.setObjectName("label_img_info")
        self.horizontalLayout.addWidget(self.label_img_info)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        main_window.setCentralWidget(self.centralWidget)
        self.menuBar = QtWidgets.QMenuBar(main_window)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 735, 22))
        self.menuBar.setObjectName("menubar")
        main_window.setMenuBar(self.menuBar)
        self.statusBar = QtWidgets.QStatusBar(main_window)
        self.statusBar.setObjectName("statusbar")
        main_window.setStatusBar(self.statusBar)

        self.retranslation_ui(main_window)
        QtCore.QMetaObject.connectSlotsByName(main_window)

    def retranslation_ui(self, main_window):
        _translate = QtCore.QCoreApplication.translate
        main_window.setWindowTitle(_translate("MainWindow", "Справка по элементу"))
        self.label_name_info.setText(_translate("MainWindow", ""))
        self.label_text_info.setText(_translate("MainWindow", ""))
        self.label_img_info.setText(_translate("MainWindow", ""))


class UiMainWindow:  # дизайн для основного окна
    def __init__(self):
        self.centralWidget = None
        self.gridLayout = None
        self.statusBar = None
        self.menuBar = None
        self.horizontalLayout = None
        self.label_img_info = None
        self.label_text_info = None
        self.label_name_info = None
        self.verticalLayout = None
        self.groupBox_question = None
        self.groupBox_main_page = None
        self.groupBox_result_check_quesion = None
        self.pushButton_next_question = None
        self.pushButton_start = None
        self.pushButton_check_answer = None
        self.pushButton_end_train = None
        self.verticalLayout_3 = None
        self.verticalLayout_2 = None
        self.verticalLayout_10 = None
        self.verticalLayout_7 = None
        self.verticalLayout_8 = None
        self.verticalLayout_1 = None
        self.main_img_label = None
        self.horizontalLayout_1 = None
        self.label_number_element = None
        self.label_result = None
        self.label_img_info = None
        self.label_name_element = None
        self.lineEdit_answer = None
        self.groupBox_choose_elements = None
        self.radioButton_choose_all = None
        self.radioButton_choose_period = None
        self.radioButton_choose_group = None
        self.radioButton_group1 = None
        self.radioButton_group2 = None
        self.radioButton_group3 = None
        self.radioButton_group4 = None
        self.radioButton_group5 = None
        self.radioButton_group6 = None
        self.radioButton_group7 = None
        self.radioButton_group8 = None
        self.radioButton_period1 = None
        self.radioButton_period2 = None
        self.radioButton_period3 = None
        self.radioButton_period4 = None
        self.radioButton_period5 = None
        self.radioButton_period6 = None
        self.radioButton_period7 = None
        self.radioButton_period8 = None
        self.label = None
        self.gridLayout2 = None
        self.verticalLayout_33 = None
        self.verticalLayout_55 = None
        self.horizontalLayout_37 = None
        self.buttonGroup_choose_group = None
        self.buttonGroup_choose_elements = None
        self.buttonGroup_choose_period = None
        self.label_error = None
        self.horizontalLayout_23 = None
        self.pushButton_next_from_choose_elements = None

    def setup_ui(self, main_window):
        main_window.setObjectName("Помощник по химии")
        main_window.setEnabled(True)
        main_window.resize(1062, 601)
        size_policy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding,
                                            QtWidgets.QSizePolicy.Expanding)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(main_window.sizePolicy().hasHeightForWidth())
        main_window.setSizePolicy(size_policy)
        main_window.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.centralWidget = QtWidgets.QWidget(main_window)
        self.centralWidget.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.centralWidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.groupBox_main_page = QtWidgets.QGroupBox(self.centralWidget)
        self.groupBox_main_page.setEnabled(True)
        self.groupBox_main_page.setTitle("")
        self.groupBox_main_page.setObjectName("groupBox_main_page")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupBox_main_page)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setContentsMargins(50, 10, 50, 10)
        self.verticalLayout.setObjectName("verticalLayout")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setContentsMargins(40, -1, 40, -1)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.verticalLayout_1 = QtWidgets.QVBoxLayout()
        self.verticalLayout_1.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_1.setObjectName("verticalLayout_1")
        self.main_img_label = QtWidgets.QLabel(self.groupBox_main_page)
        size_policy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum,
                                            QtWidgets.QSizePolicy.Minimum)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(self.main_img_label.sizePolicy().hasHeightForWidth())
        self.main_img_label.setSizePolicy(size_policy)
        self.main_img_label.setMinimumSize(QtCore.QSize(100, 100))
        self.main_img_label.setText("")
        self.main_img_label.setScaledContents(False)
        self.main_img_label.setAlignment(QtCore.Qt.AlignCenter)
        self.main_img_label.setObjectName("main_img_label")
        self.verticalLayout_1.addWidget(self.main_img_label)
        self.verticalLayout_3.addLayout(self.verticalLayout_1)
        self.pushButton_start = QtWidgets.QPushButton(self.groupBox_main_page)
        self.pushButton_start.setObjectName("pushButton_start")
        self.verticalLayout_3.addWidget(self.pushButton_start)
        self.verticalLayout.addLayout(self.verticalLayout_3)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.gridLayout.addWidget(self.groupBox_main_page, 0, 0, 1, 1)

        self.groupBox_question = QtWidgets.QGroupBox(self.centralWidget)
        self.groupBox_question.setEnabled(True)
        self.groupBox_question.setTitle("")
        self.groupBox_question.setObjectName("groupBox_question")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupBox_question)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setContentsMargins(80, 10, 85, 10)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.horizontalLayout_1 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_1.setContentsMargins(20, 20, 20, 20)
        self.horizontalLayout_1.setObjectName("horizontalLayout_1")
        self.label_number_element = QtWidgets.QLabel(self.groupBox_question)
        font = QtGui.QFont()
        font.setPointSize(23)
        self.label_number_element.setFont(font)
        self.label_number_element.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_number_element.setAlignment(QtCore.Qt.AlignCenter)
        self.label_number_element.setObjectName("label_number_element")
        self.horizontalLayout_1.addWidget(self.label_number_element)
        self.label_name_element = QtWidgets.QLabel(self.groupBox_question)
        font = QtGui.QFont()
        font.setPointSize(23)
        self.label_name_element.setFont(font)
        self.label_name_element.setAlignment(QtCore.Qt.AlignCenter)
        self.label_name_element.setObjectName("label_name_element")
        self.horizontalLayout_1.addWidget(self.label_name_element)
        self.verticalLayout_7.addLayout(self.horizontalLayout_1)
        self.verticalLayout_8 = QtWidgets.QVBoxLayout()
        self.verticalLayout_8.setContentsMargins(140, -1, 140, -1)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.lineEdit_answer = QtWidgets.QLineEdit(self.groupBox_question)
        self.lineEdit_answer.setMinimumSize(QtCore.QSize(100, 20))
        self.lineEdit_answer.setStyleSheet("")
        self.lineEdit_answer.setText("")
        self.lineEdit_answer.setObjectName("lineEdit_answer")
        self.verticalLayout_8.addWidget(self.lineEdit_answer)
        self.pushButton_check_answer = QtWidgets.QPushButton(self.groupBox_question)
        self.pushButton_check_answer.setMinimumSize(QtCore.QSize(100, 20))
        self.pushButton_check_answer.setObjectName("pushButton_check_answer")
        self.verticalLayout_8.addWidget(self.pushButton_check_answer)
        self.verticalLayout_7.addLayout(self.verticalLayout_8)
        self.verticalLayout_2.addLayout(self.verticalLayout_7)
        self.gridLayout.addWidget(self.groupBox_question, 0, 0, 1, 1)

        self.groupBox_result_check_quesion = QtWidgets.QGroupBox(self.centralWidget)
        self.groupBox_result_check_quesion.setEnabled(True)
        self.groupBox_result_check_quesion.setTitle("")
        self.groupBox_result_check_quesion.setObjectName("groupBox_result_check_quesion")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupBox_result_check_quesion)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout_10 = QtWidgets.QVBoxLayout()
        self.verticalLayout_10.setContentsMargins(100, 10, 105, 10)
        self.verticalLayout_10.setObjectName("verticalLayout_10")
        self.label_result = QtWidgets.QLabel(self.groupBox_result_check_quesion)
        font = QtGui.QFont()
        font.setPointSize(16)
        self.label_result.setFont(font)
        self.label_result.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_result.setStyleSheet("color: green")
        self.label_result.setAlignment(QtCore.Qt.AlignCenter)
        self.label_result.setObjectName("label_result")
        self.verticalLayout_10.addWidget(self.label_result)
        self.pushButton_end_train = QtWidgets.QPushButton(self.groupBox_result_check_quesion)
        self.pushButton_end_train.setObjectName("pushButton_end_train")
        self.verticalLayout_10.addWidget(self.pushButton_end_train)
        self.pushButton_next_question = QtWidgets.QPushButton(self.groupBox_result_check_quesion)
        self.pushButton_next_question.setMinimumSize(QtCore.QSize(100, 20))
        self.pushButton_next_question.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.pushButton_next_question.setObjectName("pushButton_next_question")
        self.verticalLayout_10.addWidget(self.pushButton_next_question)
        self.verticalLayout_2.addLayout(self.verticalLayout_10)
        self.gridLayout.addWidget(self.groupBox_result_check_quesion, 0, 0, 1, 1)

        self.groupBox_choose_elements = QtWidgets.QGroupBox(self.centralWidget)
        self.groupBox_choose_elements.setTitle("")
        self.groupBox_choose_elements.setObjectName("groupBox_choose_elements")
        self.gridLayout2 = QtWidgets.QGridLayout(self.groupBox_choose_elements)
        self.gridLayout2.setObjectName("gridLayout")
        self.verticalLayout_33 = QtWidgets.QVBoxLayout()
        self.verticalLayout_33.setContentsMargins(10, 10, 10, 10)
        self.verticalLayout_33.setObjectName("verticalLayout_33")
        self.label = QtWidgets.QLabel(self.groupBox_choose_elements)
        self.label.setObjectName("label")
        self.verticalLayout_33.addWidget(self.label)
        self.radioButton_choose_all = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_choose_all.setObjectName("radioButton_choose_all")
        self.verticalLayout_33.addWidget(self.radioButton_choose_all)
        self.horizontalLayout_23 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_23.setObjectName("horizontalLayout_23")
        self.radioButton_choose_period = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_choose_period.setObjectName("radioButton_choose_period")
        self.horizontalLayout_23.addWidget(self.radioButton_choose_period)
        self.radioButton_choose_group = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_choose_group.setObjectName("radioButton_choose_group")
        self.horizontalLayout_23.addWidget(self.radioButton_choose_group)
        self.verticalLayout_33.addLayout(self.horizontalLayout_23)
        self.horizontalLayout_37 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_37.setObjectName("horizontalLayout_37")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.radioButton_period1 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_period1.setObjectName("radioButton_period1")
        self.verticalLayout_2.addWidget(self.radioButton_period1)
        self.radioButton_period2 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_period2.setObjectName("radioButton_period2")
        self.verticalLayout_2.addWidget(self.radioButton_period2)
        self.radioButton_period3 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_period3.setObjectName("radioButton_period3")
        self.verticalLayout_2.addWidget(self.radioButton_period3)
        self.radioButton_period4 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_period4.setObjectName("radioButton_period4")
        self.verticalLayout_2.addWidget(self.radioButton_period4)
        self.radioButton_period5 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_period5.setObjectName("radioButton_period5")
        self.verticalLayout_2.addWidget(self.radioButton_period5)
        self.radioButton_period6 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_period6.setObjectName("radioButton_period6")
        self.verticalLayout_2.addWidget(self.radioButton_period6)
        self.radioButton_period7 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_period7.setObjectName("radioButton_period7")
        self.verticalLayout_2.addWidget(self.radioButton_period7)
        self.horizontalLayout_37.addLayout(self.verticalLayout_2)
        self.verticalLayout_55 = QtWidgets.QVBoxLayout()
        self.verticalLayout_55.setObjectName("verticalLayout_55")
        self.radioButton_group1 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_group1.setObjectName("radioButton_group1")
        self.verticalLayout_55.addWidget(self.radioButton_group1)
        self.radioButton_group2 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_group2.setObjectName("radioButton_group2")
        self.verticalLayout_55.addWidget(self.radioButton_group2)
        self.radioButton_group3 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_group3.setObjectName("radioButton_group3")
        self.verticalLayout_55.addWidget(self.radioButton_group3)
        self.radioButton_group4 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_group4.setObjectName("radioButton_group4")
        self.verticalLayout_55.addWidget(self.radioButton_group4)
        self.radioButton_group5 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_group5.setObjectName("radioButton_group5")
        self.verticalLayout_55.addWidget(self.radioButton_group5)
        self.radioButton_group6 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_group6.setObjectName("radioButton_group6")
        self.verticalLayout_55.addWidget(self.radioButton_group6)
        self.radioButton_group7 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_group7.setObjectName("radioButton_group7")
        self.verticalLayout_55.addWidget(self.radioButton_group7)
        self.radioButton_group8 = QtWidgets.QRadioButton(self.groupBox_choose_elements)
        self.radioButton_group8.setObjectName("radioButton_group8")
        self.verticalLayout_55.addWidget(self.radioButton_group8)
        self.horizontalLayout_37.addLayout(self.verticalLayout_55)
        self.verticalLayout_33.addLayout(self.horizontalLayout_37)
        self.gridLayout2.addLayout(self.verticalLayout_33, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.groupBox_choose_elements)
        self.pushButton_next_from_choose_elements = QtWidgets.QPushButton(self.groupBox_choose_elements)
        self.pushButton_next_from_choose_elements.setObjectName("pushButton_next_from_choose_elements")
        self.label_error = QtWidgets.QLabel(self.groupBox_choose_elements)
        self.label_error.setText("")
        self.label_error.setObjectName("label_error")
        self.verticalLayout_33.addWidget(self.label_error)
        self.verticalLayout_33.addWidget(self.pushButton_next_from_choose_elements)

        self.buttonGroup_choose_elements = QtWidgets.QButtonGroup(main_window)
        self.buttonGroup_choose_elements.setObjectName("buttonGroup_choose_elements")
        self.buttonGroup_choose_elements.addButton(self.radioButton_choose_all)
        self.buttonGroup_choose_elements.addButton(self.radioButton_choose_period)
        self.buttonGroup_choose_elements.addButton(self.radioButton_choose_group)

        self.buttonGroup_choose_group = QtWidgets.QButtonGroup(main_window)
        self.buttonGroup_choose_group.setObjectName("buttonGroup_choose_group")
        self.buttonGroup_choose_group.addButton(self.radioButton_group1)
        self.buttonGroup_choose_group.addButton(self.radioButton_group2)
        self.buttonGroup_choose_group.addButton(self.radioButton_group3)
        self.buttonGroup_choose_group.addButton(self.radioButton_group4)
        self.buttonGroup_choose_group.addButton(self.radioButton_group5)
        self.buttonGroup_choose_group.addButton(self.radioButton_group6)
        self.buttonGroup_choose_group.addButton(self.radioButton_group7)
        self.buttonGroup_choose_group.addButton(self.radioButton_group8)

        self.buttonGroup_choose_period = QtWidgets.QButtonGroup(main_window)
        self.buttonGroup_choose_period.setObjectName("buttonGroup_choose_period")
        self.buttonGroup_choose_period.addButton(self.radioButton_period1)
        self.buttonGroup_choose_period.addButton(self.radioButton_period2)
        self.buttonGroup_choose_period.addButton(self.radioButton_period3)
        self.buttonGroup_choose_period.addButton(self.radioButton_period4)
        self.buttonGroup_choose_period.addButton(self.radioButton_period5)
        self.buttonGroup_choose_period.addButton(self.radioButton_period6)
        self.buttonGroup_choose_period.addButton(self.radioButton_period7)

        main_window.setCentralWidget(self.centralWidget)
        self.menuBar = QtWidgets.QMenuBar(main_window)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 1062, 22))
        self.menuBar.setObjectName("menubar")
        main_window.setMenuBar(self.menuBar)
        self.statusBar = QtWidgets.QStatusBar(main_window)
        self.statusBar.setObjectName("statusbar")
        main_window.setStatusBar(self.statusBar)

        self.retranslation_ui(main_window)
        QtCore.QMetaObject.connectSlotsByName(main_window)

    def retranslation_ui(self, main_window):
        _translate = QtCore.QCoreApplication.translate
        main_window.setWindowTitle(_translate("MainWindow", "Помощник по химии"))
        self.pushButton_start.setText(_translate("MainWindow", "Начать тренировку"))
        self.label_number_element.setText(_translate("MainWindow", "Порядковый номер\n\n1"))
        self.label_name_element.setText(_translate("MainWindow", "Обозначение\n\nH"))
        self.pushButton_check_answer.setText(_translate("MainWindow", "Проверить"))
        self.label_result.setText(_translate("MainWindow", "Правильных ответ!\n"
                                                           "1 из 5"))
        self.pushButton_end_train.setText(_translate("MainWindow", "Закончить тренировку"))
        self.pushButton_next_question.setText(_translate("MainWindow", "Дальше"))
        self.label.setText(_translate("MainWindow", "Сделайте выборку элементов"))
        self.radioButton_choose_all.setText(_translate("MainWindow", "Без выборки(10 случайных элементов)"))
        self.radioButton_choose_period.setText(_translate("MainWindow", "По периоду"))
        self.radioButton_choose_group.setText(_translate("MainWindow", "По группе"))
        self.radioButton_period1.setText(_translate("MainWindow", "1"))
        self.radioButton_period2.setText(_translate("MainWindow", "2"))
        self.radioButton_period3.setText(_translate("MainWindow", "3"))
        self.radioButton_period4.setText(_translate("MainWindow", "4"))
        self.radioButton_period5.setText(_translate("MainWindow", "5"))
        self.radioButton_period6.setText(_translate("MainWindow", "6"))
        self.radioButton_period7.setText(_translate("MainWindow", "7"))
        self.radioButton_group1.setText(_translate("MainWindow", "1"))
        self.radioButton_group2.setText(_translate("MainWindow", "2"))
        self.radioButton_group3.setText(_translate("MainWindow", "3"))
        self.radioButton_group4.setText(_translate("MainWindow", "4"))
        self.radioButton_group5.setText(_translate("MainWindow", "5"))
        self.radioButton_group6.setText(_translate("MainWindow", "6"))
        self.radioButton_group7.setText(_translate("MainWindow", "7"))
        self.radioButton_group8.setText(_translate("MainWindow", "8"))
        self.pushButton_next_from_choose_elements.setText(_translate("MainWindow", "Дальше"))


class Window2(QMainWindow, UiWindow2):  # механика окна со справкой об элементе
    def __init__(self, number_of_element=0):
        super().__init__()
        self.setup_ui(self)
        # подключаем базу данных
        self.main_database = sqlite3.connect("Tables/MendeleevTable.sqlite")
        self.main_database_cur = self.main_database.cursor()
        # id элемента, для которого нужна справка
        self.id = number_of_element
        # заполняем все поля
        self.set_name_and_number_element()
        self.set_text_info()
        self.set_img()

        self.number = None
        self.name = None
        self.designation = None
        self.text_info = None
        self.pixmap = None
        self.group = None
        self.period = None

    def set_name_and_number_element(self):
        """Заполняет название, обозначение, порядковый номер элемента"""
        result = self.main_database_cur.execute("""SELECT * FROM Mendeleev
                    WHERE id = ?""", (self.id,)).fetchall()
        self.number = result[0][3]  # порядковый номер
        self.name = result[0][2]  # название
        self.designation = result[0][1]  # обозначение
        self.period = result[0][4]  # период
        self.group = result[0][5]  # группа
        text = f'Название: {self.name}\nПорядковый номер: {self.number}\nОбозначение:' \
               f' {self.designation}\nПериод: {self.period}\nГруппа: {self.group}'
        self.label_name_info.setText(text)

    def set_text_info(self):
        """Заполняет краткую информацию об элементе"""
        result = self.main_database_cur.execute("""SELECT * FROM Information
                    WHERE id = ?""", (self.id,)).fetchall()
        self.text_info = result[0][1]
        self.label_text_info.setText(self.text_info)

    def set_img(self):
        """Ставит картинку элемента"""
        if os.path.exists(f'images/{self.name}.jpg'):
            self.pixmap = QPixmap(f'images/{self.name}.jpg')
            self.label_img_info.setPixmap(self.pixmap)
        elif os.path.exists(f'images/{self.name}.png'):
            self.pixmap = QPixmap(f'images/{self.name}.png')
            self.label_img_info.setPixmap(self.pixmap)


class MainWindow(QMainWindow, UiMainWindow):  # механика основного окна
    def __init__(self):
        super().__init__()
        self.setup_ui(self)
        self.setFixedSize(769, 537)

        # все элементы разделены на группы со сценами
        # показываем элементы первой сцены
        self.groupBox_result_check_quesion.setEnabled(False)
        self.groupBox_result_check_quesion.setVisible(False)

        self.groupBox_question.setEnabled(False)
        self.groupBox_question.setVisible(False)

        self.groupBox_choose_elements.setEnabled(False)
        self.groupBox_choose_elements.setVisible(False)

        self.groupBox_main_page.setVisible(True)
        self.groupBox_main_page.setEnabled(True)

        self.pushButton_start.clicked.connect(self.start)
        self.pushButton_check_answer.clicked.connect(self.check_answer)
        self.pushButton_next_question.clicked.connect(self.next_question)
        self.pushButton_end_train.clicked.connect(self.end_train)

        self.pixmap = QPixmap('images/img.jpg')
        self.main_img_label.setPixmap(self.pixmap)

        # подключаем базы данных
        self.main_database = sqlite3.connect("Tables/MendeleevTable.sqlite")
        self.main_database_cur = self.main_database.cursor()

        self.coordinates_database = sqlite3.connect('Tables/coordinatesOfElements.sqlite')
        self.coordinates_cur = self.coordinates_database.cursor()

        self.counter_of_correct_answers = 0
        self.count_answers = 0

        self.setMouseTracking(True)
        self.x = 0
        self.y = 0
        self.event = None

        self.result = None
        self.window2 = None

        self.choose_period_res = None
        self.choose_group_res = None
        self.choose_elements_res = None
        self.questions_created = False
        self.questions = []
        self.is_training = False

        self.buttonGroup_choose_elements.buttonClicked.connect(self.choose_elements)
        self.buttonGroup_choose_group.buttonClicked.connect(self.choose_group)
        self.buttonGroup_choose_period.buttonClicked.connect(self.choose_period)
        self.pushButton_next_from_choose_elements.clicked.connect(self.next_from_choose_elements)

    def start(self):
        """Переходит к тренировке"""
        # показываем следующую сцену
        self.groupBox_main_page.setVisible(False)
        self.groupBox_main_page.setEnabled(False)
        self.is_training = True
        self.groupBox_choose_elements.setEnabled(True)
        self.groupBox_choose_elements.setVisible(True)
        self.pushButton_next_question.setVisible(True)
        self.pushButton_next_question.setEnabled(True)

    def check_answer(self):
        """Проверяет ответ"""
        # показываем сцену с проверкой ответа
        self.groupBox_question.setEnabled(False)
        self.groupBox_question.setVisible(False)
        self.groupBox_result_check_quesion.setEnabled(True)
        self.groupBox_result_check_quesion.setVisible(True)
        self.count_answers += 1
        if ''.join(self.lineEdit_answer.text().split()).capitalize() == self.result[2]:
            self.counter_of_correct_answers += 1
            self.label_result.setText(
                f"Правильных ответ!\n\nПравильных ответов:\n" +
                f"{self.counter_of_correct_answers} из {self.count_answers}")
            self.label_result.setStyleSheet("color: green")
        else:
            self.label_result.setText(
                f"Неправильный ответ!\n\nПравильных ответов:\n" +
                f"{self.counter_of_correct_answers} из {self.count_answers}")
            self.label_result.setStyleSheet("color: red")

    def next_question(self):
        """Показывает следующий вопрос"""
        # проверяем, остались ли еще вопросы в тренировке
        if len(self.questions) > 0:
            # переходим к следующему вопросу
            self.create_question()
            self.groupBox_result_check_quesion.setVisible(False)
            self.groupBox_result_check_quesion.setEnabled(False)
            self.groupBox_question.setEnabled(True)
            self.groupBox_question.setVisible(True)
        else:
            # заканчиваем тренировку
            self.label_result.setText(f"Тренировка закончена\n\nПравильных ответов:"
                                      f"\n{self.counter_of_correct_answers} из {self.count_answers}")
            self.label_result.setStyleSheet("color: black")
            self.pushButton_next_question.setVisible(False)
            self.pushButton_next_question.setEnabled(False)
        self.lineEdit_answer.setText('')

    def end_train(self):
        """Возвращает на главное окно и заканчивает тренировку"""
        # Возвращаемся на первую страницу
        self.groupBox_main_page.setVisible(True)
        self.groupBox_main_page.setEnabled(True)
        self.groupBox_result_check_quesion.setEnabled(False)
        self.groupBox_result_check_quesion.setVisible(False)

        self.counter_of_correct_answers = 0
        self.lineEdit_answer.setText('')
        self.count_answers = 0
        self.questions = []
        self.questions_created = False
        self.is_training = False
        self.choose_elements_res = None
        self.choose_group_res = None
        self.choose_period_res = None
        # обнуляем кнопки для выборки элементов
        for i in self.buttonGroup_choose_period.buttons():
            self.buttonGroup_choose_period.setExclusive(False)
            i.setChecked(False)
            i.repaint()
            self.buttonGroup_choose_period.setExclusive(True)
        for i in self.buttonGroup_choose_group.buttons():
            self.buttonGroup_choose_group.setExclusive(False)
            i.setChecked(False)
            i.repaint()
            self.buttonGroup_choose_group.setExclusive(True)
        for i in self.buttonGroup_choose_elements.buttons():
            self.buttonGroup_choose_elements.setExclusive(False)
            i.setChecked(False)
            i.repaint()
            self.buttonGroup_choose_elements.setExclusive(True)

    def mousePressEvent(self, event):
        """По координатам нажатия мышкой находит в таблице элемент"""
        self.event = event.button()
        self.x = int(event.x())
        self.y = int(event.y())
        result = self.coordinates_database.execute("""SELECT * FROM coordinates
                    WHERE top_left_coordinate_x < ? and ? < bottom_right_coordinate_x
                     and top_left_coordinate_y < ? and ? < bottom_right_coordinate_y""",
                                                   (self.x, self.x, self.y, self.y)).fetchall()
        if result and not self.is_training:
            id_element = int(result[0][0])
            # создаем новое окно
            self.window2 = Window2(number_of_element=id_element)
            self.window2.show()

    def choose_elements(self, btn):
        """Нажатия на кнопки для выборки элементов"""
        self.label_error.setText('')
        if btn == self.radioButton_choose_all:
            for i in self.buttonGroup_choose_period.buttons():
                i.setEnabled(False)
            for i in self.buttonGroup_choose_group.buttons():
                i.setEnabled(False)
        elif btn == self.radioButton_choose_period:
            for i in self.buttonGroup_choose_period.buttons():
                i.setEnabled(True)
            for i in self.buttonGroup_choose_group.buttons():
                i.setEnabled(False)
        elif btn == self.radioButton_choose_group:
            for i in self.buttonGroup_choose_period.buttons():
                i.setEnabled(False)
            for i in self.buttonGroup_choose_group.buttons():
                i.setEnabled(True)
        self.choose_elements_res = btn

    def choose_period(self, btn):
        """Нажатие на период для выборки"""
        self.choose_period_res = btn

    def choose_group(self, btn):
        """Нажатие на группу для выборки"""
        self.choose_group_res = btn

    def next_from_choose_elements(self):
        """Переходим к вопросам от выборки элементов"""
        if self.choose_elements_res is not None and self.choose_elements_res != self.radioButton_choose_all:
            if self.choose_period_res is not None or self.choose_group_res is not None:
                self.groupBox_choose_elements.setEnabled(False)
                self.groupBox_choose_elements.setVisible(False)
                self.create_question()
            else:
                self.label_error.setText('Заполните все поля')
        elif self.choose_elements_res is not None and self.choose_elements_res == self.radioButton_choose_all:
            self.groupBox_choose_elements.setEnabled(False)
            self.groupBox_choose_elements.setVisible(False)
            self.create_question()

        elif self.choose_elements_res is None:
            self.label_error.setText('Выберите все нужные поля')

    def create_question(self):
        """Создает список вопросов, если его нет, и выбирает вопрос"""
        self.groupBox_question.setVisible(True)
        self.groupBox_question.setEnabled(True)
        if self.choose_elements_res == self.radioButton_choose_all:
            if not self.questions_created:
                # при первом входе в функцию создаем список вопросов
                self.questions = []
                numbers = random.choices(list(range(1, 110)), k=10)
                # находим нужную строку в таблице
                for i in numbers:
                    self.questions += self.main_database_cur.execute("""SELECT * FROM Mendeleev
                                    WHERE Number = ?""", (i,)).fetchall()
                self.questions_created = True
        elif self.choose_group_res is not None:
            if not self.questions_created:
                # при первом входе в функцию создаем список вопросов
                self.questions = []
                number_of_group = int(self.choose_group_res.text()[-1])
                # находим нужную строку в таблице

                self.questions = self.main_database_cur.execute("""SELECT * FROM Mendeleev
                                    WHERE GroupOfElement = ?""", (number_of_group,)).fetchall()
                self.questions_created = True
        elif self.choose_period_res is not None:
            if not self.questions_created:
                # при первом входе в функцию создаем список вопросов
                self.questions = []
                number_of_period = int(self.choose_period_res.text()[-1])
                # находим нужную строку в таблице
                self.questions = self.main_database_cur.execute("""SELECT * FROM Mendeleev
                                    WHERE Period = ?""", (number_of_period,)).fetchall()
                self.questions_created = True
        # берем один из вопросов из списка
        self.result = self.questions.pop()
        self.label_name_element.setText(f"Обозначение\n\n{self.result[1]}")
        self.label_number_element.setText(f"Порядковый номер\n\n{self.result[0]}")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    ex.show()
    sys.exit(app.exec())
